<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Dane</title>

    <style>
        * {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: sans-serif;
            font-size: 16px;
        }

        .submit {
            margin-top: 0.5rem;
        }

        .center {
            display: flex;
            justify-content: center;
        }

        #employee {
            margin-top: 0.5rem;
            margin-bottom: 0.5rem;
        }

        .card {
            margin: 1rem auto;
            padding: 1rem;
            border-radius: 3px;
            background-color: #CCD1D9;
            max-width: 380px;

            box-shadow: 0 0 1rem #555;
        }

        .header {
            text-align: center;
            margin: .25rem .25rem;
            padding: 0.1rem 0.1rem;
        }

        .item {
            margin 1.25rem 0.5rem;
        }

        select {
            margin: 0 0.5rem;
            width: 175px;
        }

        input[type=submit] {
            margin: 0.5rem 1rem;
            width: 140px;
        }
    </style>
</head>
<body>
    <?php
        $connection = pg_connect("host=sbazy dbname=s187127 user=s187127 password=aG5AYhGE");
        $query = 'SELECT id_pracownika, nazwisko, imie FROM _pracownicy ORDER BY nazwisko';
        $result = pg_exec($connection, $query);

        $rows = pg_numrows($result);
    ?>

    <div class="card">
        <div class="header">
            <h3>Lista wszystkich pracowników</h3>
        </div>
        <div class="item">
            <form method='post' action='respond.php'>
                <div id='employee' class='center'>
                    <p>Wybierz pracownika:</p>
                    <select name='employees'>
                        <?php
                            for ($row = 0; $row < $rows; $row++) {
                                $id = pg_result($result, $row, 0);
                                $surname = pg_result($result, $row, 1);
                                $name = pg_result($result, $row, 2);
                                print "<option name='employee' value='$id'>[$id] $surname, $name</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="submit center"><input type='submit' value='Wybierz'></div>
            </form>
        </div>
    </div>

</body>
</html>