<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Pracownik</title>
    <style>
        body {
            font-family: sans-serif;
            font-size: 16px;
            margin: 0;
            padding: 0;
        }

        .header h3 {
            margin-top: 0.5rem;
            text-align: center;
        }

        .center {
            display: flex;
            justify-content: center;
        }

        .card {
            margin: 1rem;
            padding: 1rem;
            border-radius: 3px;
            background-color: #CCD1D9;
            width: 380px;

            box-shadow: 0 0 1rem #555;
        }
    </style>
</head>
<body>

    <?php
        //Pobieranie danych z bazy
        $connection = pg_connect("host=sbazy dbname=s187127 user=s187127 password=aG5AYhGE");

        $id = $_POST['employees'];
        $query2 = "SELECT * FROM (_pracownicy LEFT JOIN _wykladowcy ON id_pracownika=id_wykladowcy) LEFT JOIN _stopnie_tytuly USING(stopien_tytul) WHERE id_pracownika='$id'";
        $result2 = pg_exec($connection, $query2);

        //dane samego pracownika
        $id_pracownika = pg_result($result2, 0, 1);
        $nazwisko = pg_result($result2, 0, 2);
        $imie = pg_result($result2, 0, 3);
        $nip = pg_result($result2, 0, 4);
        $pesel = pg_result($result2, 0, 5);

        //dane gdy jest wykladowcą
        $stopienTytul = pg_result($result2, 0, 0);
        $id_wykladowcy = pg_result($result2, 0, 6);
        $katedra = pg_result($result2, 0, 7);
        $stawka = pg_result($result2, 0, 8);

        $nip = $nip == null ? "Brak NIPu" : $nip;
        $pesel = $pesel == null ? "Brak PESELu" : $pesel;

        $employee = ["Nazwisko" => $nazwisko, "Imię" => $imie, "NIP" => $nip, "PESEL" => $pesel];
        if ($id_wykladowcy != null) {
            $employee["Stopień"] = $stopienTytul;
            $employee["Katedra"] = $katedra;
            $employee["Stawka"] = $stawka;
        }

    ?>

    <div class="center">
        <div class="card">
            <div class="header">
                <h3>Dane pracownika</h3>
            </div>
            <div class="content">
                <?php
                    foreach ($employee as $key => $value) {
                        print "<p>$key: $value</p>";
                    }
                ?>
            </div>
        </div>
    </div>
</body>
</html>







